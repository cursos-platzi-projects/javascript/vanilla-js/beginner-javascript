class Toggable {
  //el se refiere a la abreviatura de "Elemento" pero se le puede llamar de otra manera
  constructor(el) {
    this.el = el
    this.el.innerHTML = 'Off'
    this.activated = false
    this.onClick = this.onClick.bind(this)
    this.el.addEventListener('click', this.onClick)
  }

  //Funcion para cambiar el estado
  onClick() {
    this.activated = !this.activated
    this.toggleText()
  }

  //Funcion para cambiar el texto
  toggleText() {
    //Esto es como un if lineal
    this.el.innerHTML = this.activated ? 'On' : 'Off'
  }

}

const button = document.getElementById('boton')

const miBoton = new Toggable(button)
