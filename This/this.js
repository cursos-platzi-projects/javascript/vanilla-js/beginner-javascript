class Persona {
  constructor(nombre, amigos = []) {
    this.nombre = nombre
    this.amigos = amigos
  }

//La funcion de esta manera da un error por el acceso al "this"
/*Este modo es con los arrow function y te evitas de poner bind o
declarar otra variable asignadole al this*/
  listarAmigos() {
    this.amigos.forEach((amigo) => {
      console.log(`Hola, mi nombre es ${this.nombre} y soy amigo de ${amigo}`)
    })
  }

  //Asignacion de variable _this al this
  /*listarAmigos() {
    const _this = this //Esta es una manera de que funcione el nombre
    this.amigos.forEach(function (amigo) {
      console.log(`Hola, mi nombre es ${_this.nombre} y soy amigo de ${amigo}`)
    })
  }*/

  //Una manera de hacerlo con bind
  /*listarAmigos() {
    this.amigos.forEach(function (amigo) {
      console.log(`Hola, mi nombre es ${this.nombre} y soy amigo de ${amigo}`)
    }.bind(this))
  }*/
}

const gianni = new Persona("Gianni", ["Pedro", "Juan", "Pepe"])
