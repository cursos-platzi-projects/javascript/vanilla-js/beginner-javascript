/*Para numeros muy grandes se utiliza memoizacion*/
/*Esto es para que javascript no ejecute la funcion miles de veces, si no solo unos cuantos*/
let contadorMemo = 1
function fibonacciMemo(num, memoria = {}) {
  contadorMemo++
  if (memoria[num]) return memoria[num]
  if (num == 1) return 0
  if (num == 2) return 1

  return memoria[num] = fibonacciMemo(num - 1, memoria) + fibonacciMemo(num - 2, memoria)
}
//Recursivo sin memoria genera mas procesos que el anterior
let contadorRec = 1
function fibonacciRecursivo(num) {
  contadorRec++
  if (num == 1) return 0
  if (num == 2) return 1

  return fibonacciRecursivo(num - 1) + fibonacciRecursivo(num - 2)
}
