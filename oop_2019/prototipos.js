//En esta funcion se explica como hacer la herecia entre funciones o prototipos
//NOTA: Esta es una manera mas complicada, hay otra manera mas sencilla
//NOTA: Hay que tener en cuanta que en Javascript no hay clases, no existen, para eso se les llama protitipos y exista la herencia prototipal para la herencia entre prototipos
function heredaDe(prototipoHijo, prototipoPadre) {
    //Se crea una funcion anonita con la variabla 'fn'
    var fn = function () {} 
    prototipoHijo.prototype = new fn
    //Para asignarle el constructor al hijo
    prototipoHijo.prototype.constructor = prototipoHijo
}

function Persona (nombre, apellido, altura) {
    this.nombre = nombre
    this.apellido = apellido
    this.edad = 20
    this.altura = altura
}

//Para crear un metodo de la clase Persona
Persona.prototype.saludar = function() {
    console.log(`Hola, me llamo ${this.nombre} ${this.apellido}`)
}

Persona.prototype.soyAlto = function() {
    return this.altura > 1.8
} 

/*In console you can test it putting:
erika
erika.saludar()
*/

//-----------------------
/**Modificar un prototipo
 * Es recomendable declarar hasta arriva los prototipos antes de ser declarados por que si no dara error de que no existen tales funciones.
*/

/**El contexto de this */
//Cuando se declara un arrow function, la palabra this ya no hace referencia a los atributos de la clase, si no hace referencia a lo global:
//De esta manera, este this ya no hace referencia a la clase Persona con sus atributos si no a Window que es la variable global
Persona.prototype.soyAlto = () => {
    return this.altura > 1.8
}

/**Herencia en prototipos */

function Desarrollador(nombre, apellido) {
    this.nombre = nombre;
    this.apellido = apellido;
}

heredaDe(Desarrollador, Persona)

Desarrollador.prototype.saludar = function() {
    console.log(`Hola, soy ${this.nombre} ${this.apellido} y soy desarrollador/a`)
}

/*var gianni = new Persona('Gianni', 'Otro', 1.72)
var erika = new Persona('Erika', 'Luna', 1.65)
var arturo = new Persona('Arturo', 'Martinez', 1.89)*/
