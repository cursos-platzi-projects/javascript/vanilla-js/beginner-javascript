//Ejecutar esto con un archivo html, por si solo no se puede
/*En este codigo hacemos una peticion de una API solicitando
ciertos datos, esto es un ejemplo de los callbacks a un servidor externo*/

/*const promise = new Promise(function (resolve, reject) {
  setTimeout(resolve, 1000)
})

promise
  .then(function() {

  })
  .catch(function(err) {

  })*/


function get(URL, callback) {
  const xhr = new XMLHttpRequest();

  xhr.onreadystatechange = function() {
    const DONE = 4
    const OK = 200
    if(this.readyState === DONE) {
      if(this.status === OK) {
        //Todo ok
        callback(null, JSON.parse(this.responseText))
      } else {
        //hubo un error
        callback(new Error(`Se produjo un error al realizar el request: ${this.status}`))
      }
    }
  }

  xhr.open('GET', URL);
  xhr.send(null);
}

function handleError(err) {
  console.log(`Request failed: ${err}`);
}

get('https://swapi.co/api/people/1/', function onResponse(err, luke){
  if(err) return handleError(err)

  get(luke.homeworld, function onHomeworldResponse(err, homeworld){
    if(err) return handleError(err)
    /*De tantas peticiones al servidor podria ser como un cuello de botella o un
    callback hell y para evitar esto se hace de otra manera las cuales son las promesas*/
    luke.homeworld = homeworld
    console.log(`${luke.name} nacio en ${luke.homeworld.name}`);
  })
  /*console.log(`Request succeded`);
  console.log('luke', luke);*/
})
