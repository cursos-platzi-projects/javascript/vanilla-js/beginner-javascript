function handleError(err) {
  console.log(`Request failed: ${err}`);
}
async function getLuke() {
  /*Await lo que hace es esperar a que termina de procesar el fetch para despues
    nos devuelva el resultado en la variable res
    Esto se hace por que como fetch es una promesa lo hace de manera asincrona y por
    eso debemos esperarlo*/
    try{
      const response = await fetch('https://swapi.co/api/people/1/')
      const luke = await response.json()
      const responseHomeworld = await fetch(luke.homeworld)
      luke.homeworld = await responseHomeworld.json()
      console.log(`${luke.name} nacio en ${luke.homeworld.name}`);
    } catch(error) {
      handleError(err)
    }
}
