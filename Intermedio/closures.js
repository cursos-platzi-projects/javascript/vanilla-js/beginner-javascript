function saludarFamilia(apellido) {
  return function saludarMiembroDeFamilia(nombre) {
    console.log(`Hola ${nombre} ${apellido}`);
  }
}

const saludarGomez = saludarFamilia("Gomez")
const saludarPerez = saludarFamilia("Peres")
const saludarRomero = saludarFamilia("Romero")

saludarGomez("Pepe")
saludarGomez("Juan")
saludarGomez("Laura")
saludarGomez("Monica")

saludarPerez("Dario")
saludarPerez("Carlos")
saludarPerez("Valeria")

saludarRomero("Jose")

/*Ejercicio*/
function makePrefixer(prefijo){
  return function madePrefijo(palabra) {
    console.log(prefijo+palabra);
  }
}
const prefijoRe = makePrefixer("re")
prefijo("bueno")

/*Mejor codigo*/
makePrefixer = prefijo => palabra => prefijo + palabra;
const prefijoRe = makePrefixer("re");
prefijoRe("malo");
