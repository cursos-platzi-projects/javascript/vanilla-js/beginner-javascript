var alan = {
    nombre: 'Alan',
    apellido: 'Perez',
    altura: 1.86,
    cantidadDeLibros: 182
}

var carmen = {
    nombre: 'Carmen',
    apellido: 'Perez',
    altura: 1.40,
    cantidadDeLibros: 142
}

var juan = {
    nombre: 'Juan',
    apellido: 'Perez',
    altura: 1.71,
    cantidadDeLibros: 250
}

var pedro = {
    nombre: 'Pedro',
    apellido: 'Perez',
    altura: 1.51,
    cantidadDeLibros: 36
}

const esAlta = persona => persona.altura > 1.8
//const esAlta = ({ altura }) => altura > 1.8

var personas = [alan, carmen, juan, pedro]

//Filter = crea un array dependiendo de la condicion de datos que se obtenga en esAlta
var personasAltas = personas.filter(esAlta)


const pasarAlturaCms = persona => {
    return {
        ...persona,
        altura: persona.altura * 100
    }
}

//Map crea un nuevo array desde pasarAlturaCms
var personasCms = personas.map(pasarAlturaCms)


//-------------------------------------------------
/**Reducir un array a un valor */

//Old solution
var acum = 0;
for(var i = 0; i < personas.length; i++) {
    acum = acum + personas[i].cantidadDeLibros
}

//Better solution
//cont reducer = (acum, personas.cantidadDeLibros) => acum + personas.cantidadDeLibros
const reducer = (acum, {cantidadDeLibros}) => acum + cantidadDeLibros

var totalDeLibros = personas.reduce(reducer, 0)

console.log(`En total todos tienen ${totalDeLibros} libros`)