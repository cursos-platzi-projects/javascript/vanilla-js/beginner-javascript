//math.floor = rendondea hacia abajo 3.9 = 3
//math.ceil = redondea para arriba 3.1 = 4
//math.round =  redondea dependiendo 3.1 = 3, 3.5 = 4

const nombre = 'Gianni'
const dias = [
  "lunes",
  "martes",
  "miércoles",
  "jueves",
  "viernes",
  "sábado",
  "domingo"
]

function correr() {
  const min = 5
  const max = 15
  //Math.floor() = nos pone el valor mas bajo posible, ejemplo: 1.6 = 1, 0.60 = 0
  //Math.ceil() = redondea para arriba 3.1 = 4
  //Math.random() = Metodo para valores aleatorios
  return Math.round(Math.random() * (max - min)) + min
}

let totalKms = 0
const length = dias.length
for ( let i = 0; i < length; i++) {
  //kms = es igual a la funcion que retorna el numero aleatorio * los numeros establecidos
  const kms = correr()
  totalKms += kms
  console.log(`El día ${dias[i]} ${nombre} corrió ${kms}kms`);
}

const promedioKms = totalKms / length
//toFixed = tomar la cantidad de num decimales. Ejemplo: 9.857142857142858 = 9.85
console.log(`En promedio, ${nombre} corrió ${promedioKms.toFixed(2)}kms `);
