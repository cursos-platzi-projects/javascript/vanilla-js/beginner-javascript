function platzom(str){
  let translation = str
  //Si la palabra termina en "ar", se le quitan esos dos caracteres
  if (str.toLowerCase().endsWith('ar')) {
    //.slice = metodo para cortar caracteres
    //@params (Empieze, y corte caracteres. el -2 significa los ultimos 2)
    translation = str.slice(0,-2)
  }

  //Si la palabra inicia con Z, se le añade "pe" al final
  if (str.toLowerCase().startsWith('z')){
    translation += 'pe'
  }

  //Si la palabra traducida tiene 10 o más letras,
  //se debe partir a la mitad y unir con un guión del medio
  const length = translation.length
  if (length >= 10){
    const firstHalf = translation.slice(0, Math.round(length / 2))
    const secondHalf = translation.slice(Math.round(length / 2))
    translation = `${firstHalf}-${secondHalf}`
  }

  //reverse = Metodo para invertir la cadena
  //join = Metodo para mostrar la cadena junta y no como en un array
  //@params split = Divide la cadena dependiendo del texto que le pones, por ejemplo por ','
  const reverse = (str) => str.split('').reverse().join('')

  function minMay(str) {
    const length = str.length
    let translation = ''
    let capitalize = true
    for (let i = 0; i < length; i++) {
      //charAt = Metodo que obtiene la letra de una palabra
      const char = str.charAt(i)
      //@params (capitalize se evalua como true) ? si es true : si es else
      translation += capitalize ? char.toUpperCase() : char.toLowerCase()
      //se pone en false para ir cambiando de mayusculas a minusculas y de vuelta con toda la cadena
      capitalize = !capitalize
    }

    return translation
  }
  //Si str y reverse(str) son iguales, es por que es un palindromo y se modifica la cadena
  console.log("cadena str "+str);
  if (str == reverse(str)) {
    return minMay(str)
  }

  return translation
}

console.log(platzom("programar"))
console.log(platzom("Zorro"))
console.log(platzom("Zarpar"))
console.log(platzom("abecedario"));
console.log(platzom("sometemos"));
